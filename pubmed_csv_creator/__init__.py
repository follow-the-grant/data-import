#!/usr/bin/env python

import click
import codecs
import csv
import sys
import multiprocessing
import json
from lxml import etree
import pickle
import datetime
import re
from bs4 import BeautifulSoup
import html2text

errors_file = open('errors-{}.pickled'.format(datetime.date.today().strftime('%d-%m-%Y')), 'wb')
errors = []

def get_grant(file, filename) -> ['article_id']:
   d = parse_grant_id(file) 

def get_dates(file, filename) -> ['article_id', 'ptype', 'pdate']:
    article_id = get_article_id(file, filename)
    pub_dates = file.findall('.//pub-date')
    dates = []
    for pub_date in pub_dates:
        if 'pub-type' in pub_date.attrib:
            pub_type = pub_date.attrib['pub-type']
        else:
            pub_type = 'NONE'
        ysearch = pub_date.find('.year')
        year = ysearch.text.strip() if ysearch is not None else '1900'

        msearch = pub_date.find('.month')
        month = msearch.text.strip() if msearch is not None else '01'

        dsearch = pub_date.find('.day')
        day =  dsearch.text.strip() if dsearch is not None else '01'
        if day:
            pdate = '{}-{:02d}-{:02d}'.format(year, int(month), int(day))
        else:
            pdate = '{}-{:02d}-{:02d}'.format(year, int(month), 1)
        dates += [article_id + [pub_type, pdate]]
    return dates

def get_contributions(file, filename) -> ['article_id', 'role', 'institution_uid', 'institution_name', 'name', 'surname']:
    # AUTHORS
    contributors = file.findall('.//contrib')
    contributors_list = []
    article_id = get_article_id(file, filename)
    nd = {}

    # AFFILIATIONS
    affiliations = {}
    institutions = {}
    affs = file.findall('.//aff')

    for aff in affs:
        addr_line = aff.find('addr-line')
        if 'id' not in aff.attrib:
            pass
        else:
            institutions[aff.attrib['id']] = get_institution(filename, aff)
            has_label = False
            if aff.find('label') is not None:
                has_label = True

            aff_address_parts = [a for a in aff.xpath('text()') if a != '\n']
            if has_label:
                aff_address_parts = aff_address_parts[1:]
            affiliations[aff.attrib['id']] = ' '.join(aff_address_parts)

    for contributor in contributors:
        name = contributor.find('./name/given-names')
        name_text = name.text if name is not None else ''
        surname = contributor.find('./name/surname')
        surname_text = surname.text if surname is not None else ''

        roles = contributor.findall('role')
        cont = {}
        cont['type'] = contributor.attrib.get('contrib-type', 'NONE')

        # ROLES
        cont['roles'] = []
        for role in roles:
            cont['roles'] += [role.text]

        # AFFILIATIONS
        cont_affs = contributor.findall('.//xref[@ref-type="aff"]')
        cont['affiliations_ids'] = []
        try:
            for cont_aff in cont_affs:
                cont['affiliations_ids'] += [cont_aff.attrib.get('rid', 'NONE')]
        except:
            errors += [{'error': 'gettting affiliation id', 'params': [file]}]

        for aff in cont['affiliations_ids']:
            contributors_list += [article_id + [ cont['type']] + institutions.get(aff, ["",""]) + [ name_text, surname_text]]

    return contributors_list

def get_institution(filename, affiliation):
    institution_wrap = affiliation.find('.//institution-wrap')
    if institution_wrap is not None:
        inst_ids = {inst_id.attrib.get('institution-id-type', 'NONE'): inst_id.text for inst_id in institution_wrap.findall('./institution-id')}
        institution = []
        for inst in institution_wrap.findall('./institution'):
            if inst.text:
                institution += [inst.text.replace('\n', ' ').replace('\t', ' ')]

        return [json.dumps(inst_ids), ' | '.join(institution)]
    else:
        institution = affiliation.find('institution')
        if institution is None:
           aff_address_parts = [a.replace('\n', ' ') for a in affiliation.xpath('text()') if a != '\n']
           institution = ' '.join(aff_address_parts)
        else:
            institution = institution.text
        return ['NONE', institution.replace('\n', ' ').replace('\t', ' ')]
    return ['NONE', '']

def get_authors_names(file, filename) -> ['name', 'surname']:
    # AUTHORS
    contributors = file.findall('.//contrib')
    contributors_list = []
    for contributor in contributors:
        name = contributor.find('./name/given-names')
        name_text = name.text if name is not None else ''
        surname = contributor.find('./name/surname')
        surname_text = surname.text if surname is not None else ''
        contributors_list += [[name_text, surname_text]]
    return contributors_list

def get_doi(file, filename) -> ['doi']:
    doi_search = file.find('.//article-id[@pub-id-type="doi"]')
    if doi_search is not None:
        doi = doi_search.text
    else:
        doi = ''
    return [doi]

def parse_coi_statements(tree, filename) -> ['article_id', 'coi_type', 'coi_text']:
    """
    Parse conflict of interest statements from given article tree
    """
    #article_id = get_article_id(tree, filename)

    coi_paths = [
        (1, 'conflict'),
        (2, 'CoiStatement'),
        (3, './/*[@*="conflict"]'),
        (4, './/*[@*="conflict-interest"]'),
        (5, './/*[@*="COI-statement"]'),
        (6, './/*[contains(translate(text(), "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),"interest") and (contains(translate(text(), "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),"competing") or contains(translate(text(), "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),"declaring") or contains(translate(text(), "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),"conflict"))]')
        ]

    coi_text = ''
    coi = ''
    
    for path in coi_paths:
        for el in tree.xpath(path[1]):
            coi = '\n'.join(el.itertext())
            coi_text = coi.replace('\t', ' ').replace('\n', ' ')

            if path[0] == 6 and len(coi_text) <= 36:
                article_text = html2text(codecs.open(filename, 'r', encoding='utf8').read())
                match = re.search(coi_text, article_text, flags=re.IGNORECASE) 
                if match is not None:
                    start_pos = match.start()
                    full_coi_text = ''
                    not_newline = True
                    i = 0
                    while not_newline:
                        char = article_text[start_pos + i]
                        if char == '\n' and article_text[start_pos + i + 1] == '\n' and i > len(coi_text):
                            not_newline = False   
                        else:
                            full_coi_text += article_text[start_pos + i]
                            i += 1
                    coi_text = full_coi_text.replace('\t', ' ').replace('\n', ' ')

        if coi_text != '':
            return [filename, str(path[0]), coi_text]

    return [filename, -1, '']

def get_coi(file, filename) -> ['article_id', 'coi_type', 'coi_text']:
    article_id = get_article_id(file, filename)
    # COI (NEW)
    e = file.findall('.//fn[@fn-type="COI-statement"]')
    coi_type = 'NONE'
    coi = etree.tostring(e[0], method='text', encoding='unicode') if len(e) > 0 else 'NONE'

    # COI (OLD)
    if coi == 'NONE':
        e = file.find('conflict')
        if e:
            coi = etree.tostring(e[0], method='text', encoding='unicode') if len(e) > 0 else 'NONE'
            coi = coi.replace('"', '""')
            if coi != 'NONE':
                coi_type = 'OLD'
    else:
        coi_type = 'NEW'

    if coi == 'NONE':
        # Coi in notes
        note = file.xpath('.//*[@notes-type="conflict-interest"]')
        if len(note) == 0:
            note = file.xpath('.//*[@notes-type="COI-statement"]')
        else:
            coi_type = "NOTES_CONFLICT_INTEREST" 
        
        if len(note) > 0:

            coi_type = "NOTES_COI" 
            #nt = note[0].xpath('.//*[contains(translate(text(), "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),"conflict")]')
            coi_parts = []
            for n in note[0]:
                coi_parts.append(n.text)
            coi = ' '.join(coi_parts)

    if coi == 'NONE':
        tags_with_conflict = file.xpath('.//*[contains(translate(text(), "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),"conflict")]')
        if len(tags_with_conflict) > 0:
            coi = []
            for tag in tags_with_conflict:
                if tag.text is not None and len(tag.text) > 21:
                    coi += [tag.text]
            coi = ' | '.join(coi)
            coi_type = 'ARTICLE_BODY'

    return article_id + [coi_type, coi.replace('\t', ' ').replace('\n', ' ')]

def get_fund_text(article, filename) -> ['fund_text']:
    fund = 'NONE'
    # FUND
    ef =  article.findall('.//funding-statement')
    if len(ef) > 0:
        fund = etree.tostring(ef[0], method='text', encoding='unicode') if len(ef) > 0 else 'NONE'
        fund = fund.replace('"', '""')
    return [fund.replace('\n', '')]

def get_journal_id(article, filename) -> ['journal_id']:
    return [filename.split('/')[-2]]

def get_title_abstract(article, filename) -> ['title', 'abstract']:
    # TITLE
    title = ''
    et = article.findall('.//article-title')
    if len(et) > 0:
        title = etree.tostring(et[0], method='text', encoding='unicode')
        title = title.replace('"', '""')

    # ABSTRACT
    ea = article.findall('.//abstract')
    if ea:
        abstract = etree.tostring(ea[0], method='text', encoding='unicode')
        abstract = abstract.replace('"', '""')
    else:
        abstract = 'NONE'

    return [title.replace('\n', '').replace('\t', ''), abstract.replace('\n', '').replace('\t', '')]


def get_article_id(article, filename) -> ["article_id"]:
    filename = filename.strip()
    pmc_code = filename.strip().split('/')[-1].split('.')[-2][3:]
    return [pmc_code]


def get_institution_id(article, filename) -> ['id-type', 'id']:
    article_id = get_article_id(article, filename)
    ids = article.findall('.//aff/*/institution-id')
    ids_cols = []
    if ids:
        for i in ids:
            ids_cols += [article_id + [i.attrib.get('institution-id-type', 'NONE'), i.text]]
        return ids_cols
    else:
        return [article_id + ["",""]]


def create_csv_parallel(col_functions, file_part, csv_filename, journal_files, rows=False):
    csv_file = open(csv_filename, 'w')
    csv_writer = csv.writer(csv_file, delimiter='\t')

    if file_part == 0:
        csv_writer.writerow([
            col for func in col_functions
            for col in func.__annotations__['return']]
        )

    c = 0
    errors = []

    for i, file in enumerate(journal_files):
        try:
            filename = file.strip()
            pfile = etree.fromstring(codecs.open(filename, 'r', encoding='utf8').read())
            c += 1
            # pfile: Parsed XML file
            row = []
            for cf in col_functions:
                if not rows:
                    row += cf(pfile, filename)
                else:
                    for row in cf(pfile, filename):
                        csv_writer.writerow(row)
                        #print('\t'.join(row))

            if not rows:
                #print('\t'.join(row))
                csv_writer.writerow(row)

        except Exception as e:
            filename = file.strip()
            errors += [(csv_filename, file_part, filename)]
            continue

    pickle.dump(errors, errors_file)
    errors_file.flush()

def create_csv(col_functions, file_part, csv_filename, journal_files, rows=False):
    c = 0
    errors = []

    for file in journal_files:
        try:
            filename = file.strip()
            pfile = etree.fromstring(codecs.open(filename, 'r', encoding='utf8').read())
            c += 1
            # pfile: Parsed XML file
            row = []
            for cf in col_functions:
                if not rows:
                    row += cf(pfile, filename)
                else:
                    for row in cf(pfile, filename):
                        print('\t'.join(row))

            if not rows:
                print('\t'.join(row))

        except Exception as e:
            filename = file.strip()
            errors += [(csv_filename, file_part, filename)]
            continue

    pickle.dump(errors, errors_file)
    errors_file.flush()


@click.command()
@click.option('--metadata',
              default='ardic')
@click.option('--batches',
              default=2,
              help='The person to greet.')
@click.argument('articles_list')
def run_parallel_csv_creator(batches, metadata, articles_list):
    journal_files = open(articles_list).readlines()
    jf_len = len(journal_files)//batches
    jobs = []

    i = 22

    for i in range(batches):

        # Extract articles title, DOI, abstract and journal
        if 'a' in metadata:
            col_functions = [get_article_id, get_doi, get_fund_text, get_title_abstract, get_journal_id]
            p = multiprocessing.Process(target=create_csv_parallel, args=(col_functions, i, 'articles-{}'.format(i), journal_files[i*jf_len: (i+1)*jf_len]))
            jobs.append(p)
            p.start()
    
        # Extract contributors and institutions from each article
        if 'r' in metadata:
            col_functions = [get_contributions]
            p = multiprocessing.Process(target=create_csv_parallel, args=(col_functions, i, 'contributions-{}'.format(i), journal_files[i*jf_len: (i+1)*jf_len]))
            jobs.append(p)
            p.start()
    
        # Extract article publication dates
        if 'd' in metadata:
            col_functions = [get_dates]
            p = multiprocessing.Process(target=create_csv_parallel, args=(col_functions, i, 'dates-{}'.format(i), journal_files[i*jf_len: (i+1)*jf_len],True))
            jobs.append(p)
            p.start()
    
        # Extract institution information from articles researchers
        if 'i' in metadata:
            col_functions = [get_institution_id]
            p = multiprocessing.Process(target=create_csv_parallel, args=(col_functions, i, 'institutions-{}'.format(i), journal_files[i*jf_len: (i+1)*jf_len]))
            jobs.append(p)
            p.start()
    
        # Extract COI from articles
        if 'c' in metadata:
            col_functions = [get_coi, get_journal_id]
            p = multiprocessing.Process(target=create_csv_parallel, args=(col_functions, i, 'coi-{}'.format(i), journal_files[i*jf_len: (i+1)*jf_len]))
            jobs.append(p)
            p.start()

#        # Extract Grant from articles
#        if 'g' in metadata:
#            col_functions = [get_grant]
#            p = multiprocessing.Process(target=create_csv, args=(col_functions, i, 'GRANT-{}'.format(i), journal_files[i*jf_len: (i+1)*jf_len]))
#            jobs.append(p)
#            p.start()

@click.command()
@click.option('--metadata',
              default='ardic')
@click.option('--batches',
              default=2,
              help='The person to greet.')
@click.argument('articles_list', nargs=-1)
def run_single_csv_creator(batches, metadata, articles_list):
    journal_files = articles_list

    i = 22
    # Extract articles title, DOI, abstract and journal
    if 'a' in metadata:
        col_functions = [get_article_id, get_doi, get_fund_text, get_title_abstract, get_journal_id]
        create_csv(col_functions, i, 'ARTICLES-{}'.format(i), sys.stdin)

    # Extract contributors and institutions from each article
    if 'r' in metadata:
        col_functions = [get_contributions]
        create_csv(col_functions, i, 'CONTRIBUTIONS-{}'.format(i), sys.stdin, True)

    # Extract article publication dates
    if 'd' in metadata:
        col_functions = [get_dates]
        create_csv(col_functions, i, 'DATES-{}'.format(i), sys.stdin, True)

    # Extract institution information from articles researchers
    if 'i' in metadata:
        col_functions = [get_institution_id]
        create_csv(col_functions, i, 'INSTITUTION-ID-{}'.format(i), sys.stdin, True)

    # Extract COI from articles
    if 'c' in metadata:
        col_functions = [parse_coi_statements, get_journal_id]
        create_csv(col_functions, i, 'COI-{}'.format(i), sys.stdin)


